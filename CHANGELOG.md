## v0.4.9

- Added reference to AppInstance for ErrorException
- Added `unexpected_data` error code

## v0.4.8

- Allow empty new operator in chat transferred event

## v0.4.7

- Added `name` for `MediaFile` to allow passing display name
- Added `file_name` property for `messenger_types.*.document` schema, which controls whether transport allows to send custom file display name

## v0.4.6

- Rename `refreshTemporaryData` to `freshTemporaryData`

## v0.4.5

- Fix `OperatorGroup` interface

## v0.4.4

- Add `externalItem` feature description to schema

## v0.4.3

- Make locale & tz optional in AppIntegrationData

## v0.4.2

- Added `updateStatus` method on app instance

## v0.4.0

- Added new enums and data types to control app & app payment status
- Added new required method for AppHandler -- `status` which must return current app external status
- Added `features` field to schema

## v0.3.1

- Added new properties to schema that describe what data is available for resources

## v0.3.0

- Removed `schema` from AppHandler interface
- Added `schema` option to `Hub::registerAppType`