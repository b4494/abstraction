## Storing extra data

Almost each entity has `extraData` property where you can save your data and use later. `extraData` can be any type value. Most of the time this is an array of values. It must follow these rules:

- object keys must be as short as possible (ideally, 1-3 symbols). To keep code readbility, one may use constants that will hold short property names.

**ATTENTION** `null`s, empty arrays and empty strings are removed when `extraData` is saved.

### Updating data

If your `extraData` is an array, its data is merged on updates (i.e. when dispatching events). Following rules are applied:

- If key in new data is missing, it is kept is current data (one level deep)
- Every value in new data overwrites value in current data
- To clear value in current data, pass key with null value

For example, if we have following current data:

```json
{"foo":"bar","array":[1,2,3]}
```

If we update it with data:

```json
{"baz":"test"}
```

We'll get:

```json
{"foo":"bar","array":[1,2,3],"baz":"test"}
```

If we pass:

```json
{"array":[],"baz": "another value"}
```

We'll get:

```json
{"foo":"bar","baz":"another value"}
```

## Handling OAuth, JWT tokens or other temporary data for application

If your integration requires oAuth authentication which is temporary and therefore
cannot be stored permanently, there is other method. There is a concept of _temporary data_.

- Set property `temporaryData` of `AppIntegrationData`. This is any data you need along with the time when it expires. Data format is up to you;
- Implement `Interfaces\Features\SupportsTemporaryData`. It has method for updating temp data: it accepts current data and must return new data. New data __overwrites__ previous data. This method __must not__ be called directly, or data will be lost;
- Use `AppInstance::getTemporaryData()` to get this data. It will be automatically updated if data has expired. `forceRefresh` can be set to `true` to force data refresh;

This method is _thread safe_ which means that correct data will be available across different threads.

## Variables

Variables are defined for users to get some value. There are many internal variables that are available. Each module can define its own variables, that provide some extra info. Variables can get data from `extraData`, calculate value or maybe retrieve it from external system by HTTP-request. Users can see a list of variables. Variables have they key, name, data type and resolver.

Variable key is the main identifier of the variable. Keys must be camelCase. Keys must be unique. Users can access variable by its key.

Name of the variable is a display name that will be shown to a user. Name must be localized based on current language.

Data type designates the type of the value, what operators and functions are available for the variable. There are many data types available. Adding new data types is not possible yet.

Resolver is a callback, that is called when user wants variable value. This callback receives `Interfaces\RuntimeContext` and must return value or `null` if it's not available.

### Defining variables

Variables are defined in `Hub::registerAppType::$options`: `registerVariables` and `registerInstanceVariables` callbacks. First method is used for registering variables that are same for all users that are using the module. Second method is used for registering variables that are available only for particular user. For example, user defined some custom fields in external system, and we want to display those fields. Each user will have its own variables keys and names.

Those methods receive `VariableRegistrar`. This object has several methods that correspond to a different data types. Let's add text variable:

```php
Hub::registerAppType('myApp', static fn () => ..., [
    'registerVariables' => static function (VariablesRegistrar $registrar) {
        $registrar->text('extra_info', static fn (RuntimeContext $ctx) => $ctx->chat()->getExtraData()['extraInfo'])->name('Some extra info on chat');
    }
]);
```

We have defined a variable with key `extra_info` and with a name of _Some extra info on chat_.

#### Array variables

To define an array of strings, do following:

```php
$registrar->text('str_array', static fn (RuntimeContext $ctx) => $ctx->chat()->getExtraData()['str_array'])->name('Some strings')->array();
```

We applied `array` modifier.

#### Mixed data

You can use `mixed` type to define variable that may be of any type. Use `array` modifier if you are passing an object or an array.

#### Complex objects

It is possible to register a variable that has nested properties. It is done using `complex` type. For example lets describe deal info, that we get from chat's extra data:

```php
$registrar->complex('deal', static function (VariableRegistrar $registrar) {
    $registrar->pk('id');
    $registrar->pk('name');
    $registrar->enum('status', [
        [ 'value' => 'new', 'label' => 'New' ],
        [ 'value' => 'archive', 'label' => 'Archived' ],
    ]);
    $registrar->datetime('endsAt', 'ends_at');
}, static fn (RuntimeContext $ctx) => $ctx->chat()->getExtraData()['deal']);
```

Here we've defined `deal`, `deal.id`, `deal.name`, `deal.status` variables. When users requests `deal.status`, we resolve `deal` first by getting its data from chat's extra data. This value is then passed to the resolver for `deal.status` which is null. Null resolver means that we will use key of the variable to get the value. In our example deal is simple hash object and status is a key of that object. So, `deal.status` is resolved to field `status` of object `deal`.

#### Extending objects

Some modules can add extra data to internal resources like chat, contact and operator. These resources are complex objects:

- contact
- operator

To add new properties, just use dot when registering variables:

```php
$registrar->text('contact.sex', static fn (Contact $contact) => $contact->getExtraData()['sex']);
```

We have added property `sex` to our contact. Note that resolver gets `Contact` rather than `RuntimeContext`.

### Context flags

Variables are displayed to user in a specific context. And some variables are available only when specific flag is set. For example, variable that gets inbox text, is not shown, unless `clientResponse` flag is set. This flag indicates that in current context it is possible to receive inbox message.

- `clientResponse` -- context can have inbox message data
- `actions.*` -- in current context specified action is called
- `eventListener.*` -- in current context specified event listener is added
  - `inbox`
  - `outbox`
  - `inlineButton`
  - `quickReply`
  - `inlineUrlButton`
  - `phoneButton`
  - `locationButton`
  - `dialogClosed`
  - `dialogTransferred`
  - `invoicePaid`
  - `externalRequest`

## Logging

One may use default Laravel's logging subsystem. But additional context must be provided if present:

- `appInstance` -- module instance that is currently being used
- `appType` -- if app instance is unavailable

Examples:

```php
Log::error('Could not get contents from url ' . $file->url . ' with file_get_contents', [
    'appInstance' => $this->module->user,
]);

Log::warning('Failed to resolve app by callback', [
    'callbackId' => $callbackId,
    'appType' => 'umnico',
]);
```