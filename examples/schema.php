<?php

return [
    // The list of events that integration is able to generate
    // InboxReceived is not in the list because this is the required functionality
    'events' => [
        // Specify which flags are possible when inbox is received
        'inboxFlags' => [
            'newChatCreated' => false,
            'newTicketOpened' => false,
            'externalPostComment' => false,
        ],
        'outboxSent' => false,
        'newContactChatAdded' => false,
        'chatTicketClosed' => false,
        'chatTransferred' => false,
        'chatTagAttached' => false,
        'chatTagDetached' => false,
        'chatTicketTagAttached' => false,
        'chatTicketTagDetached' => false,
        'messageStatusChanged' => false,
        'contactDataUpdated' => false,
        'externalItem' => false,
    ],

    'features' => [
        'operators' => [
            'read' => false,
            'transfer' => false,
        ],

        'operatorGroups' => [
            'read' => false,
            'transfer' => false,
        ],

        'tags' => [
            'read' => false,
            'create' => false,
            'asStrings' => false,
            'attach' => false,
            'detach' => false,
            'attachForTicket' => false,
            'detachForTicket' => false,
        ],
    ],

    'data' => [
        'integration' => [
            'timezone' => false,
            'locale' => false,
            'users' => false,
            'currency' => false,
        ],

        'user' => [
            'locale' => false,
            'avatar' => false,
        ],

        'contact' => [
            'phone' => false,
            'email' => false,
            'avatar' => false,
            'messengerId' => false,
        ],

        'chat' => [
            'messengerId' => false,
            'operator' => true,
        ],

        'operator' => [
            'phone' => false,
            'email' => false,
            'avatar' => false,
        ],

        'messengerInstance' => [
            'messengerId' => false,
        ],

        'externalItem' => [
            'externalId' => false,
            'url' => false,
            'title' => false,
        ],
    ],

    'actions' => [
        'send_text' => [
            'open_ticket' => false,
        ],

        'send_system_message' => [
            'supported' => false,
            'max_length' => 1000,
        ],

        'update_contact_data' => [
            'supported' => false,
            'title' => 'mymodule::Change lead properties',

            'editableFields' => [
                'type' => 'object',
                'properties' => [
                    'first_name' => [
                        'type' => 'string',
                        'title' => 'mymodule::first_name',
                        'variableRef' => 'contact.firstName',
                    ],
                ],
                'processor' => 'MyModule::addDynamicProperties',
            ],
        ]
    ],

    'custom_events' => [
        [
            'id' => 'myCustomEvent',
            'title' => 'Мое событие',
            'icon' => 'balloon-heart-fill', // https://icons.getbootstrap.com/
        ]
    ],

    'custom_actions' => [
        [
            'id' => 'myCustomAction',
            'title' => 'Мое действие',
            'icon' => 'gear', // https://icons.getbootstrap.com/
            'args' => [
                'type' => 'object',
                'properties' => [
                    'text' => [
                        'type' => 'string',
                        'title' => 'Param title',
                        'description' => 'Param descriptions',
                        'example' => 'Input placeholder',
                        'default' => 'Default value',
                    ],

                    'query' => [
                        'type' => 'keyValue',
                        'title' => 'Опции',
                        'keyLabel' => 'Параметр',
                        'valueLabel' => 'Значение',
                    ],
                ],
                'required' => [ 'text' ],
                'processor' => 'MyModule::addDynamicProperties',
            ],
            'result' => [
                'type' => 'object',
                'properties' => [
                    'data' => [
                        'type' => 'mixed',
                        'title' => 'Результат',
                    ]
                ]
            ],
        ]
    ],

    'messenger_types' => [
        'default' => [
            'extends' => null,
            'name' => 'Unknown messenger',
            'internal_type' => null,

            'inline_buttons' => [
                'text' => false,
                'text_max_length' => 20,
                'url' => false,
                'payload' => false,
                'payload_max_length' => 255,
                'colors' => false,
                'max_count' => 0,
            ],

            'quick_replies' => [
                'text' => false,
                'text_max_length' => 20,
                'phone' => false,
                'geolocation' => false,
                'colors' => false,
                'max_count' => 0,
            ],

            'text' => [
                'max_length' => 4096,
            ],

            'image' => [
                'enabled' => false,
                'mime' => ['image/jpeg','image/png','image/gif'],
                'max_file_size' => 5 * 1024 * 1024,
                'caption' => false,
                'caption_max_length' => 1000,
            ],

            'video' => [
                'enabled' => false,
                'mime' => ['video/mp4', 'video/3gpp'],
                'max_file_size' => 50 * 1024 * 1024,
                'caption' => false,
                'caption_max_length' => 1000,
            ],

            'document' => [
                'enabled' => false,
                'mime' => ['*'],
                'max_file_size' => 50 * 1024 * 1024,
                'caption' => false,
                'caption_max_length' => 1000,
                'file_name' => false,
            ],

            'audio' => [
                'enabled' => false,
                'mime' => ['audio/ogg','audio/mpeg','audio/mp4','audio/aac'],
                'max_file_size' => 20 * 1024 * 1024,
                'caption' => false,
                'caption_max_length' => 1000,
            ],

            'voice' => [
                'enabled' => false,
                'mime' => ['audio/ogg'],
                'max_file_size' => 1024 * 1024,
                'caption' => false,
                'caption_max_length' => 1000,
            ],

            'geolocation' => [
                'enabled' => false,
                'caption' => false,
                'caption_max_length' => 1000,
            ],
        ],

        'my_messenger_code' => [
            'extends' => 'default',
            'name' => 'My Messenger',
            'internal_type' => 'telegram',
        ],
    ],
];