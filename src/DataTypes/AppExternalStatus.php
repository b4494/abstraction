<?php

namespace BmPlatform\Abstraction\DataTypes;

use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\PaymentStatus;
use Carbon\Carbon;

class AppExternalStatus
{
    public function __construct(
        public readonly AppStatus      $status,
        public readonly ?PaymentStatus $paymentStatus = null,
        public readonly ?Carbon        $expiresAt = null,
        public readonly ?string        $externalTariffId = null,
    ) {
        //
    }
}