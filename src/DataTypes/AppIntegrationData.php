<?php

namespace BmPlatform\Abstraction\DataTypes;

use BmPlatform\Abstraction\Enums\AccountType;
use BmPlatform\Abstraction\Enums\PaymentType;
use Carbon\CarbonTimeZone;
use Illuminate\Contracts\Support\Arrayable;

class AppIntegrationData implements Arrayable
{
    public function __construct(
        /** This is the global app type that is registered by the other side */
        public readonly string $type,

        /** User id on app's side */
        public readonly string $externalId,

        /** Account name */
        public readonly string $name,

        /** Two-letter locale (ru, en, etc) */
        public readonly ?string $locale = null,

        public readonly ?CarbonTimeZone $timeZone = null,

        public readonly ?string $email = null,
        public readonly mixed $extraData = null,
        public readonly ?TemporaryData $temporaryData = null,

        public readonly PaymentType $paymentType = PaymentType::Internal,
        public readonly ?AppExternalStatus $status = null,
        public readonly ?AccountType $accountType = AccountType::Full,

        public readonly ?array $subPartnersChain = null,
        public readonly ?string $currency = null,
    ) {
        //
    }

    public function toArray()
    {
        return [
            'type' => $this->type,
            'externalId' => $this->externalId,
            'name' => $this->name,
            'locale' => $this->locale,
            'timeZone' => $this->timeZone->getName(),
            'email' => $this->email,
            'extraData' => $this->extraData,
            'temporaryData' => $this->temporaryData?->toArray(),
        ];
    }
}