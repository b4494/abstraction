<?php

namespace BmPlatform\Abstraction\DataTypes;

use Illuminate\Contracts\Support\Arrayable;

class Chat implements Arrayable
{
    public function __construct(
        public readonly string $externalId,
        public readonly MessengerInstance|string $messengerInstance,
        public readonly Contact|string|null $contact = null,
        public readonly Operator|string|null $operator = null,
        public readonly ?string $messengerId = null,
        public readonly mixed $extraData = null,
    ) {
        //
    }

    public function toArray()
    {
        return [
            'externalId' => $this->externalId,
            'messengerInstance' => is_string($this->messengerInstance) ? $this->messengerInstance : $this->messengerInstance->toArray(),
            'contact' => is_null($this->contact) ? null : (is_string($this->contact) ? $this->contact : $this->contact->toArray()),
            'operator' => is_null($this->operator) ? null : (is_string($this->operator) ? $this->operator : $this->operator->toArray()),
            'messengerId' => $this->messengerId,
            'extraData' => $this->extraData,
        ];
    }
}