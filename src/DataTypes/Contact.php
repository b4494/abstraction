<?php

namespace BmPlatform\Abstraction\DataTypes;

class Contact extends Person
{
    public function __construct(
        public string  $externalId,
        ?string        $name = null,
        ?string        $phone = null,
        ?string        $email = null,
        ?string        $avatarUrl = null,
        public ?array  $extraFields = null,
        public ?string $messengerId = null,
        public mixed   $extraData = null,
    ) {
        parent::__construct($name, $phone, $email, $avatarUrl);
    }

    public function toArray()
    {
        return parent::toArray() + [
            'externalId' => $this->externalId,
            'extraFields' => $this->extraFields,
            'extraData' => $this->extraData,
        ];
    }
}