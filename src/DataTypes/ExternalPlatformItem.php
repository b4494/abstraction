<?php

namespace BmPlatform\Abstraction\DataTypes;

use Illuminate\Contracts\Support\Arrayable;

/**
 * This is for specifying an item on external platform like social network post
 * or an ad.
 */
class ExternalPlatformItem implements Arrayable
{
    public function __construct(
        public readonly ?string $externalId = null,
        public readonly ?string $url = null,
        public readonly ?string $title = null,
        public readonly mixed   $extraData = null,
    ) {
        //
    }

    public function toArray()
    {
        return [
            'externalId' => $this->externalId,
            'url' => $this->url,
            'title' => $this->title,
            'extraData' => $this->extraData,
        ];
    }
}