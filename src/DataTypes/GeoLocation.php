<?php

namespace BmPlatform\Abstraction\DataTypes;

use Illuminate\Contracts\Support\Arrayable;

class GeoLocation implements Arrayable
{
    public function __construct(public readonly string $lat, public readonly string $long)
    {
        //
    }

    public function toArray()
    {
        return [
            'lat' => $this->lat,
            'long' => $this->long,
        ];
    }
}