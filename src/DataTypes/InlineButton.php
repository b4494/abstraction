<?php

namespace BmPlatform\Abstraction\DataTypes;

use BmPlatform\Abstraction\Enums\ButtonColor;
use Illuminate\Contracts\Support\Arrayable;

class InlineButton implements Arrayable
{
    public function __construct(
        public readonly string $text,
        public readonly string $payload,
        public readonly ?ButtonColor $color = null,
    ) {
        //
    }

    public function toArray()
    {
        return [
            'text' => $this->text,
            'payload' => $this->payload,
            'color' => $this->color?->name,
        ];
    }
}