<?php

namespace BmPlatform\Abstraction\DataTypes;

use BmPlatform\Abstraction\Enums\ButtonColor;

class InlineUrlButton extends InlineButton
{
    public function __construct(
        string $text,
        public readonly string $url,
        string $payload,
        ?ButtonColor $color = null,
    ) {
        parent::__construct($text, $payload, $color);
    }

    public function toArray()
    {
        return parent::toArray() + [
            'url' => $this->url,
        ];
    }
}