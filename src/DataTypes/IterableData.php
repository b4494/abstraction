<?php

namespace BmPlatform\Abstraction\DataTypes;

/** @template T */
class IterableData
{
    public function __construct(
        /** @var array<int, T> */
        public readonly array $data,
        public readonly mixed $cursor,
    ) {
        //
    }
}