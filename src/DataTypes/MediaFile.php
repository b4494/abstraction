<?php

namespace BmPlatform\Abstraction\DataTypes;

use BmPlatform\Abstraction\Enums\MediaFileType;
use Illuminate\Contracts\Support\Arrayable;

class MediaFile implements Arrayable
{
    public function __construct(
        public readonly MediaFileType $type,
        public readonly string $url,
        public readonly ?string $mime = null,
        public readonly ?string $name = null,
    ) {
        //
    }

    public function getExtension(): string
    {
        return mb_strtolower(pathinfo($this->url, PATHINFO_EXTENSION));
    }

    public function toArray()
    {
        return [
            'type' => $this->type->name,
            'url' => $this->url,
            'mime' => $this->mime,
            'name' => $this->name,
        ];
    }
}