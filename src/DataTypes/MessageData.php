<?php

namespace BmPlatform\Abstraction\DataTypes;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

class MessageData implements Arrayable
{
    public function __construct(
        public readonly string       $externalId,
        public readonly ?string      $text = null,
        /** @var \BmPlatform\Abstraction\DataTypes\MediaFile[] */
        public readonly ?array       $attachments = null,
        public readonly ?GeoLocation $geoLocation = null,
        public readonly ?Carbon      $date = null,
        public readonly mixed        $extraData = null,
    ) {
        //
    }

    public function toArray()
    {
        return [
            'externalId' => $this->externalId,
            'text' => $this->text,
            'attachments' => is_array($this->attachments)
                ? array_map(fn (MediaFile $file) => $file->toArray(), $this->attachments)
                : null,
            'geoLocation' => $this->geoLocation?->toArray(),
            'date' => $this->date?->toIso8601String(),
            'extraData' => $this->extraData,
        ];
    }
}