<?php

namespace BmPlatform\Abstraction\DataTypes;

use BmPlatform\Abstraction\Enums\MessengerType;
use Illuminate\Contracts\Support\Arrayable;

class MessengerInstance implements Arrayable
{
    public function __construct(
        public string         $externalType,
        public string         $externalId,
        public ?string        $name,
        public ?string        $description = null,
        public ?string        $messengerId = null,
        public ?MessengerType $internalType = null,
        public mixed          $extraData = null,
    ) {
        //
    }

    public function toArray()
    {
        return [
            'externalType' => $this->externalType,
            'externalId' => $this->externalId,
            'name' => $this->name,
            'description' => $this->description,
            'messengerId' => $this->messengerId,
            'internalType' => $this->internalType?->name,
            'extraData' => $this->extraData,
        ];
    }
}