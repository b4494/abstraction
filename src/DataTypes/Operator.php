<?php

namespace BmPlatform\Abstraction\DataTypes;

class Operator extends Person
{
    public function __construct(
        public string $externalId,
        string        $name,
        ?string       $phone = null,
        ?string       $email = null,
        ?string       $avatarUrl = null,
        public mixed  $extraData = null,
        public ?string $locale = null,
    ) {
        parent::__construct($name, $phone, $email, $avatarUrl);
    }

    public function toArray()
    {
        return parent::toArray() + [
            'externalId' => $this->externalId,
            'extraData' => $this->extraData,
        ];
    }
}