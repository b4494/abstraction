<?php

namespace BmPlatform\Abstraction\DataTypes;

use Illuminate\Contracts\Support\Arrayable;

class OperatorGroup implements Arrayable
{
    public function __construct(
        public string $externalId,
        public string $name,
        public mixed  $extraData = null,
    ) {
        //
    }

    public function toArray()
    {
        return [
            'externalId' => $this->externalId,
            'name' => $this->name,
            'extraData' => $this->extraData,
        ];
    }
}