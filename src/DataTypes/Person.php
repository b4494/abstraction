<?php

namespace BmPlatform\Abstraction\DataTypes;

use Illuminate\Contracts\Support\Arrayable;

class Person implements Arrayable
{
    public function __construct(
        public ?string $name,
        public ?string $phone = null,
        public ?string $email = null,
        public ?string $avatarUrl = null,
    ) {
        //
    }

    public function toArray()
    {
        return [
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'avatarUrl' => $this->avatarUrl,
        ];
    }
}