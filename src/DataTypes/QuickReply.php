<?php

namespace BmPlatform\Abstraction\DataTypes;

use BmPlatform\Abstraction\Enums\ButtonColor;
use BmPlatform\Abstraction\Enums\QuickReplyType;
use Illuminate\Contracts\Support\Arrayable;

class QuickReply implements Arrayable
{
    public function __construct(
        public readonly string $text,
        public readonly QuickReplyType $type = QuickReplyType::Text,
        public readonly ?ButtonColor $color = null,
    ) {
        //
    }

    public function toArray()
    {
        return [
            'text' => $this->text,
            'type' => $this->type->name,
            'color' => $this->color?->name,
        ];
    }
}