<?php

namespace BmPlatform\Abstraction\DataTypes;

use Brick\Money\Money;

readonly class ReceiptItem
{
    public function __construct(
        public string $id,
        public ?string $externalId,
        public string $name,
        public Money $amount,
        public int $quantity,
    ) {
        //
    }
}