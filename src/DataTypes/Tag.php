<?php

namespace BmPlatform\Abstraction\DataTypes;

use Illuminate\Contracts\Support\Arrayable;

class Tag implements Arrayable
{
    public function __construct(
        public readonly string $externalId,
        public readonly string $label,
        public readonly ?string $group = null,
    ) {
        //
    }

    public function toArray()
    {
        return [
            'externalId' => $this->externalId,
            'label' => $this->label,
            'group' => $this->group,
        ];
    }
}