<?php

namespace BmPlatform\Abstraction\DataTypes;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

class TemporaryData implements Arrayable
{
    public function __construct(
        public readonly mixed $data,
        public readonly Carbon $expiresAt,
    ) {
        //
    }

    public function toArray()
    {
        return [
            'data' => $this->data,
            'expiresAt' => $this->expiresAt->toIso8601String(),
        ];
    }
}