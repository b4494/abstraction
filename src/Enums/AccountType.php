<?php

namespace BmPlatform\Abstraction\Enums;

enum AccountType: string
{
    case Full = 'full';
    case CatalogOnly = 'catalog_only';
}
