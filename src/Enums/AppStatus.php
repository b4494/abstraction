<?php

namespace BmPlatform\Abstraction\Enums;

enum AppStatus: string
{
    case Active = 'active';

    // Module is frozen when not paid
    case Frozen = 'frozen';

    // Module is disabled by user or uninstalled
    case Disabled = 'disabled';
}
