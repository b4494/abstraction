<?php

namespace BmPlatform\Abstraction\Enums;

enum ButtonColor: int
{
    case Primary = 0;
    case Secondary = 1;
    case Positive = 2;
    case Negative = 3;
}