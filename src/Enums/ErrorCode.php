<?php

namespace BmPlatform\Abstraction\Enums;

enum ErrorCode: string
{
    case FeatureNotSupported = 'feature_not_supported';
    case UnexpectedData = 'unexpected_data';

    // Server errors
    case InternalServerError = 'internal_server_error';
    case TooManyRequests = 'too_many_requests';
    case BadRequest = 'bad_request';
    case ServiceUnavailable = 'service_unavailable';
    case ConnectionFailed = 'connection_failed';
    case AuthenticationFailed = 'auth_failed';
    case OperationTimedOut = 'operation_timed_out';
    case InvalidResponseData = 'invalid_response_data';
    case UnexpectedServerError = 'unexpected_server_error';
    case NotFound = 'not_found';

    // Accounting
    case AccountDisabled = 'account_suspended';
    case InsufficientFunds = 'insufficient_funds';
    case PaymentExpired = 'payment_expired';

    // Messaging
    case InvalidMessengerInstance = 'invalid_messenger_instance';
    case MessengerInstanceDisabled = 'messenger_instance_disabled';
    case ChatNotFound = 'chat_not_found';
    case CustomerNotFound = 'customer_not_found';
    case OperatorNotFound = 'operator_not_found';
    case ChatTicketAlreadyClosed = 'chat_ticket_already_closed';
    case ChatIsBanned = 'chat_is_banned';
    case OperatorAlreadyAssigned = 'operator_already_assigned';

    // Integration
    case AppNotSupported = 'app_not_supported';
    case IntegrationNotPossible = 'integration_not_possible';

    // Other
    case DataMissing = 'data_missing';
}