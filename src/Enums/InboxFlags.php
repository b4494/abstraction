<?php

namespace BmPlatform\Abstraction\Enums;

class InboxFlags
{
    /** Indicates that this inbox created new chat */
    const NEW_CHAT_CREATED      = 0b0010;

    /** Indicates that this inbox initiated new ticket (chat may not be new) */
    const NEW_TICKET_OPENED     = 0b0001;

    /** Indicates that this message is a comment to some external post */
    const EXTERNAL_POST_COMMENT = 0b0100;
}