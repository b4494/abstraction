<?php

namespace BmPlatform\Abstraction\Enums;

enum JobPriority
{
    case Low;
    case Normal;
}
