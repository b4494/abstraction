<?php

namespace BmPlatform\Abstraction\Enums;

enum MediaFileType: int
{
    case Image = 0;
    case Video = 1;
    case Document = 2;
    case Audio = 3;
    case Voice = 4;

    public static function fromMime(string $mime): static
    {
        [ $category, ] = explode('/', $mime, 2);

        return match ($category) {
            'image' => static::Image,
            'video' => static::Video,
            'audio' => static::Audio,
            default => static::Document,
        };
    }
}