<?php

namespace BmPlatform\Abstraction\Enums;

enum MessageStatus: int
{
    case Pending = 0;
    case Sent = 1;
    case FailedToSend = -1;
    case Delivered = 2;
    case Red = 3;
}