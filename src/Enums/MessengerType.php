<?php

namespace BmPlatform\Abstraction\Enums;

/**
 * Messenger type defines feature sets. Each messenger can have alternatives.
 */
enum MessengerType: string
{
    case Telegram = 'telegram';
    case Whatsapp = 'whatsapp';
    case Vk = 'vk';
    case ViberPublic = 'viber_public';
    case Facebook = 'facebook';
    case Instagram = 'instagram';
}