<?php

namespace BmPlatform\Abstraction\Enums;

enum PaymentStatus: string
{
    // Module is on free period
    case Trial = 'trial';

    // Module is paid
    case Paid = 'paid';

    // Module waits for payment
    case NotPaid = 'not_paid';
}
