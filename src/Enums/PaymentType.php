<?php

namespace BmPlatform\Abstraction\Enums;

enum PaymentType: string
{
    // Payment is controlled by internal app
    case Internal = 'internal';

    // Payment is controlled by external system, so PaymentStatus is required
    case External = 'external';
}
