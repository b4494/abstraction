<?php

namespace BmPlatform\Abstraction\Enums;

enum QuickReplyType: int
{
    case Text = 0;
    case Phone = 1;
    case Location = 2;
}