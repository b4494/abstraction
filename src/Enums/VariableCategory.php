<?php

namespace BmPlatform\Abstraction\Enums;

enum VariableCategory: string
{
    case misc = 'misc';
    case chat = 'chat';
    case contact = 'contact';
    case event = 'event';
    case payments = 'payments';
    case shop = 'shop';
    case widget = 'widget';
}