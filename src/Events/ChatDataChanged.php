<?php

namespace BmPlatform\Abstraction\Events;

use BmPlatform\Abstraction\DataTypes\Chat;
use Carbon\Carbon;

class ChatDataChanged extends Event
{
    public function __construct(public readonly Chat $chat, ?Carbon $timestamp = null)
    {
        parent::__construct($timestamp);
    }
}