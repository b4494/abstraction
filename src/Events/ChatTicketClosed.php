<?php

namespace BmPlatform\Abstraction\Events;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use Carbon\Carbon;

class ChatTicketClosed extends Event
{
    public function __construct(
        public readonly Chat|string $chat,
        public readonly ?string     $ticketId = null,
        ?Carbon                     $timestamp = null,
    ) {
        parent::__construct($timestamp);
    }
}