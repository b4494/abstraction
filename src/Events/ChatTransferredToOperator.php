<?php

namespace BmPlatform\Abstraction\Events;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use Carbon\Carbon;

class ChatTransferredToOperator extends Event
{
    public function __construct(
        public readonly Chat|string          $chat,
        /** Accepting operator data or operator external id */
        public readonly Operator|string|null $newOperator,
        public readonly Operator|string|null $prevOperator = null,
        ?Carbon                              $timestamp = null,
    ) {
        parent::__construct($timestamp);
    }
}