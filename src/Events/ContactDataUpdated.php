<?php

namespace BmPlatform\Abstraction\Events;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use Carbon\Carbon;

class ContactDataUpdated extends Event
{
    public function __construct(
        public readonly Contact $contact,
        ?Carbon                 $timestamp = null
    ) {
        parent::__construct($timestamp);
    }
}