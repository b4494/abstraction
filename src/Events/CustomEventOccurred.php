<?php

namespace BmPlatform\Abstraction\Events;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use Carbon\Carbon;

class CustomEventOccurred extends Event
{
    public function __construct(
        public readonly Chat|string $chat,
        public readonly string      $eventId,
        public readonly mixed       $eventData = null,
        ?Carbon                     $timestamp = null
    ) {
        parent::__construct($timestamp);
    }
}