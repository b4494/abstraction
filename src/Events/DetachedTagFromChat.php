<?php

namespace BmPlatform\Abstraction\Events;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Tag;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use Carbon\Carbon;

class DetachedTagFromChat extends Event
{
    public function __construct(
        public readonly Chat|string $chat,
        public readonly Tag|string  $tag,
        public readonly bool        $forTicket = false,
        ?Carbon                     $timestamp = null,
    ) {
        parent::__construct($timestamp);
    }
}