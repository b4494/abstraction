<?php

namespace BmPlatform\Abstraction\Events;

use Carbon\Carbon;

class Event
{
    public function __construct(public readonly ?Carbon $timestamp = null)
    {
        //
    }
}