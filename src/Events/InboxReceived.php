<?php

namespace BmPlatform\Abstraction\Events;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\ExternalPlatformItem;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Enums\InboxFlags;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use Carbon\Carbon;
use JetBrains\PhpStorm\ExpectedValues;

/** Received message from external system */
class InboxReceived extends Event
{
    public function __construct(
        public readonly Chat|string           $chat,
        public readonly Contact|string        $participant,
        public readonly MessageData           $message,
        public readonly ?ExternalPlatformItem $externalItem = null,
        #[ExpectedValues(flagsFromClass: InboxFlags::class)]
        public readonly ?int                  $flags = null,
        ?Carbon                               $timestamp = null,
    ) {
        parent::__construct($timestamp);
    }
}