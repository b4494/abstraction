<?php

namespace BmPlatform\Abstraction\Events;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MessageStatus;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use Carbon\Carbon;

class MessageStatusChanged extends Event
{
    public function __construct(
        public readonly Chat|string   $chat,
        public readonly string        $externalId,
        public readonly MessageStatus $status,
        ?Carbon                       $timestamp = null,
        public readonly ?ErrorCode    $errorCode = null,
        public readonly ?string       $errorDescription = null,
    ) {
        parent::__construct($timestamp);
    }
}