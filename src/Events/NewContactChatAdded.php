<?php

namespace BmPlatform\Abstraction\Events;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use Carbon\Carbon;

/**
 * This event is emitted when chat is created by operator.
 * It must not be emitted when customer opens the chat.
 */
class NewContactChatAdded extends Event
{
    public function __construct(
        public readonly Chat    $chat,
        public readonly Contact $contact,
        public readonly mixed   $extraData = null,
        ?Carbon                 $timestamp = null,
    ) {
        parent::__construct($timestamp);
    }
}