<?php

namespace BmPlatform\Abstraction\Events;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use Carbon\Carbon;

/** Message has been sent by the operator */
class OutboxSent extends Event
{
    public function __construct(
        public readonly Chat|string          $chat,
        public readonly MessageData          $message,
        public readonly Operator|string|null $operator = null,
        ?Carbon                              $timestamp = null,
    ) {
        parent::__construct($timestamp);
    }
}