<?php

namespace BmPlatform\Abstraction\Exceptions;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Interfaces\AppInstance;

class ErrorException extends \RuntimeException
{
    public function __construct(
        public readonly ErrorCode $errorCode,
        string $message = '',
        ?\Throwable $previous = null,
        public readonly ?AppInstance $appInstance = null,
    ) {
        parent::__construct($message, previous: $previous);
    }
}