<?php

namespace BmPlatform\Abstraction\Exceptions;

use BmPlatform\Abstraction\Enums\ErrorCode;
use Illuminate\Contracts\Support\MessageBag;

class ValidationException extends ErrorException
{
    public function __construct(public readonly MessageBag $errors)
    {
        parent::__construct(ErrorCode::BadRequest);
    }
}