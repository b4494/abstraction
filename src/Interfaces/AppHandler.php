<?php

namespace BmPlatform\Abstraction\Interfaces;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\Interfaces\Commands\Commands;

interface AppHandler
{
    /** @return IterableData<\BmPlatform\Abstraction\DataTypes\MessengerInstance> */
    public function getMessengerInstances(mixed $cursor = null): IterableData;

    public function getCommands(): Commands;

    /** Activate application if it was deactivated. */
    public function activate(): void;

    /** Deactivate application */
    public function deactivate(): void;

    public function status(): AppExternalStatus;
}