<?php

namespace BmPlatform\Abstraction\Interfaces;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\TemporaryData;
use BmPlatform\Abstraction\Events\Event;
use Carbon\CarbonTimeZone;

interface AppInstance extends HasExtraData
{
    public function getAppType(): string;
    public function getExternalId(): string;
    public function getHandler(): AppHandler;
    public function getLocale(): string;
    public function getTimezone(): CarbonTimeZone;
    public function isActive(): bool;

    /**
     * This method will return current temp data.
     * It will refresh data using SupportsTempData::freshTemporaryData if it is expired
     * or `$forceRefresh` is true.
     *
     * @see \BmPlatform\Abstraction\Interfaces\Features\SupportsTemporaryData::freshTemporaryData()
     */
    public function getTemporaryData(bool $forceRefresh = false): ?TemporaryData;

    public function updateStatus(AppExternalStatus $status): void;

    /**
     * @param string|resource|\Psr\Http\Message\StreamInterface $contents
     * @param string|null $name Name will be generated randomly if not specified
     * @param string|null $path File folder
     *
     * @return string Public file url
     */
    public function publishFile(mixed $contents, ?string $name = null, ?string $path = null): string;

    /**
     * @param string $name
     * @param string|null $path
     *
     * @return string|null published file url if it was published
     */
    public function publishedFileUrl(string $name, ?string $path = null): ?string;
    
    public function dispatchEvent(Event $event): void;
}