<?php

namespace BmPlatform\Abstraction\Interfaces;

interface Chat extends ExternalResource, HasExtraData
{
    public function getMessengerInstance(): MessengerInstance;
    public function getMessengerId(): ?string;
    public function getContact(): ?Contact;
    public function getOperator(): ?Operator;
}