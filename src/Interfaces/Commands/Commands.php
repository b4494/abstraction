<?php

namespace BmPlatform\Abstraction\Interfaces\Commands;

use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;

interface Commands
{
    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    public function sendTextMessage(SendTextMessageRequest $request): MessageSendResult;

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    public function sendMediaMessage(SendMediaRequest $request): MessageSendResult;
}