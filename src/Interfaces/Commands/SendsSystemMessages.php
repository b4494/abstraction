<?php

namespace BmPlatform\Abstraction\Interfaces\Commands;

use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;

interface SendsSystemMessages
{
    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    public function sendSystemMessage(SendSystemMessageRequest $request): void;
}