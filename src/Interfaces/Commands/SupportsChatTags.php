<?php

namespace BmPlatform\Abstraction\Interfaces\Commands;

use BmPlatform\Abstraction\Requests\ChatTagRequest;

interface SupportsChatTags
{
    /**
     * This command should raise an event when tag is actually attached.
     *
     * @see \BmPlatform\Abstraction\Events\AttachedTagToChat
     *
     * @param \BmPlatform\Abstraction\Requests\ChatTagRequest $request
     */
    public function attachTagToChat(ChatTagRequest $request): void;

    /**
     * This command should raise an event when tag is actually detached.
     *
     * @see \BmPlatform\Abstraction\Events\DetachedTagFromChat
     *
     * @param \BmPlatform\Abstraction\Requests\ChatTagRequest $request
     */
    public function detachTagFromChat(ChatTagRequest $request): void;
}