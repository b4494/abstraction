<?php

namespace BmPlatform\Abstraction\Interfaces\Commands;

use BmPlatform\Abstraction\Interfaces\Chat;

interface SupportsChatTickets
{
    /**
     * This action should generate an event directly or via webhook.
     *
     * @see \BmPlatform\Abstraction\Events\ChatTicketClosed
     * @param \BmPlatform\Abstraction\Interfaces\Chat $chat
     */
    public function closeChatTicket(Chat $chat): void;
}