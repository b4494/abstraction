<?php

namespace BmPlatform\Abstraction\Interfaces\Commands;

use BmPlatform\Abstraction\Interfaces\RuntimeContext;

interface SupportsCustomActions
{
    public function callCustomAction(RuntimeContext $context, string $id, array $args): ?array;
}