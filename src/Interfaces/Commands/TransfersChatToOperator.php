<?php

namespace BmPlatform\Abstraction\Interfaces\Commands;

use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;

interface TransfersChatToOperator
{
    /**
     * This command should raise an event when chat is transferred directly or via webhook event.
     *
     * @throws \BmPlatform\Abstraction\Exceptions\ErrorException
     * @see \BmPlatform\Abstraction\Events\ChatTransferredToOperator
     */
    public function transferChatToOperator(TransferChatToOperatorRequest $request): NewOperatorResponse;
}