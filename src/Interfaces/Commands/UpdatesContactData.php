<?php

namespace BmPlatform\Abstraction\Interfaces\Commands;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;

interface UpdatesContactData
{
    /**
     * This command must only update data on the other side.
     *
     * @throws \BmPlatform\Abstraction\Exceptions\ErrorException
     */
    public function updateContactData(UpdateContactDataRequest $request): ?Contact;
}