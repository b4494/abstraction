<?php

namespace BmPlatform\Abstraction\Interfaces;

interface Contact extends ExternalResource, Person, HasExtraData
{
    public function getExtraFields(): ?array;
    public function getMessengerId(): ?string;
}