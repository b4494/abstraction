<?php

namespace BmPlatform\Abstraction\Interfaces;

interface ExternalResource
{
    public function getExternalId(): string;
}