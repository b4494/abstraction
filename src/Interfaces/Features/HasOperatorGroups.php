<?php

namespace BmPlatform\Abstraction\Interfaces\Features;

use BmPlatform\Abstraction\DataTypes\IterableData;

interface HasOperatorGroups
{
    /** @return IterableData<\BmPlatform\Abstraction\DataTypes\OperatorGroup> */
    public function getOperatorGroups(mixed $cursor = null): IterableData;
}