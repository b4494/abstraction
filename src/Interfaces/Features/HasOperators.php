<?php

namespace BmPlatform\Abstraction\Interfaces\Features;

use BmPlatform\Abstraction\DataTypes\IterableData;

interface HasOperators
{
    /** @return IterableData<\BmPlatform\Abstraction\DataTypes\Operator> */
    public function getOperators(mixed $cursor = null): IterableData;
}