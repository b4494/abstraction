<?php

namespace BmPlatform\Abstraction\Interfaces\Features;

use BmPlatform\Abstraction\DataTypes\IterableData;

interface HasTags
{
    /** @return IterableData<\BmPlatform\Abstraction\DataTypes\Tag> */
    public function getTags(mixed $cursor = null): IterableData;
}