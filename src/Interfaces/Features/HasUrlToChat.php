<?php

namespace BmPlatform\Abstraction\Interfaces\Features;

use BmPlatform\Abstraction\Interfaces\Chat;

interface HasUrlToChat
{
    public function chatUrl(Chat $chat);
}