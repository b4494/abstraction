<?php

namespace BmPlatform\Abstraction\Interfaces\Features;

use BmPlatform\Abstraction\DataTypes\IterableData;

interface SupportsChatsImport
{
    /** @return \BmPlatform\Abstraction\DataTypes\IterableData<\BmPlatform\Abstraction\DataTypes\Chat> */
    public function getChats(mixed $cursor = null): IterableData;
}