<?php

namespace BmPlatform\Abstraction\Interfaces\Features;

interface SupportsOneTimePayments
{
    /**
     * @var array<\BmPlatform\Abstraction\DataTypes\ReceiptItem> $items
     * @throws \BmPlatform\Abstraction\Exceptions\ErrorException
     * @return mixed external operation id
     */
    public function getPaymentForServices(array $items): mixed;
}