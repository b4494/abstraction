<?php

namespace BmPlatform\Abstraction\Interfaces\Features;

use BmPlatform\Abstraction\DataTypes\TemporaryData;

interface SupportsTemporaryData
{
    /** This method takes old temp data and must return new temp data. This method MUST NOT update app instance. */
    public function freshTemporaryData(TemporaryData $data): TemporaryData;
}