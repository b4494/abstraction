<?php

namespace BmPlatform\Abstraction\Interfaces;

interface HasExtraData
{
    public function getExtraData(): mixed;
}