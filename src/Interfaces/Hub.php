<?php

namespace BmPlatform\Abstraction\Interfaces;

use BmPlatform\Abstraction\DataTypes\AppIntegrationData;
use BmPlatform\Abstraction\DataTypes\Operator as OperatorData;

interface Hub
{
    /**
     * Schema (`schema` option) describes module features. See example in `./examples/schema.php`.
     *
     * @param string $type
     * @param callable $createHandler Accepts AppInstance
     * @param array{
     *     schema: array|callable,
     *     registerVariables: callable,
     *     registerInstanceVariables: callable,
     * } $options
     *
     * @return void
     */
    public function registerAppType(string $type, callable $createHandler, array $options = []): void;

    public function createHandler(AppInstance $appInstance): AppHandler;

    public function integrate(AppIntegrationData $data): AppInstance;

    public function findAppById(string $type, string $id): ?AppInstance;

    /** Get the token which can be used to authenticate end users to use API/WEB. */
    public function getAuthToken(AppInstance $appInstance, OperatorData|string|null $operator = null): string;

    /** Return URL that can be used to show WEB interface. */
    public function webUrl(string $authToken, array $meta = []): string;

    /** Get string that can be used to identify app instance from webhook */
    public function getCallbackId(AppInstance $appInstance): string;

    /** Get application instance by callback id */
    public function findAppByCallbackId(string $type, string $id): ?AppInstance;
}