<?php

namespace BmPlatform\Abstraction\Interfaces;

use BmPlatform\Abstraction\Enums\MessengerType;

interface MessengerInstance extends ExternalResource, HasExtraData
{
    public function getInternalType(): ?MessengerType;
    public function getMessengerId(): ?string;
    public function getExternalType(): string;
    public function getName(): ?string;
    public function getDescription(): ?string;
}