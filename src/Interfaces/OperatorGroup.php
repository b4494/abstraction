<?php

namespace BmPlatform\Abstraction\Interfaces;

interface OperatorGroup extends HasExtraData, ExternalResource
{
    public function getName(): string;
}