<?php

namespace BmPlatform\Abstraction\Interfaces;

interface Person
{
    public function getName(): ?string;
    public function getPhone(): ?string;
    public function getEmail(): ?string;
    public function getAvatarUrl(): ?string;
}