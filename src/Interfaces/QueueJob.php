<?php

namespace BmPlatform\Abstraction\Interfaces;

use BmPlatform\Abstraction\Enums\JobPriority;
use Illuminate\Contracts\Queue\ShouldQueue;

interface QueueJob extends ShouldQueue
{
    public function getAppInstance(): AppInstance;

    public function getPriority(): JobPriority;
}