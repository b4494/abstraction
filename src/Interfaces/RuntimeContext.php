<?php

namespace BmPlatform\Abstraction\Interfaces;

interface RuntimeContext
{
    public function appInstance(): AppInstance;

    public function chat(): Chat;

    /** This will cache value in memory. Useful for heavy variables. Cache is cleared automatically.
     * Cache is stored in chat scope.
     */
    public function cacheValue(string $key, callable $callback): mixed;

    /** Data from `CustomEventOccurred::$eventData` */
    public function customEventData(): mixed;
}