<?php

namespace BmPlatform\Abstraction\Interfaces;

interface Tag extends ExternalResource
{
    public function getLabel();
}