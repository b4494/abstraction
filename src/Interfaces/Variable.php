<?php

namespace BmPlatform\Abstraction\Interfaces;

use BmPlatform\Abstraction\Enums\VariableCategory;

interface Variable
{
    /** Localized variable display name. */
    public function name(string $name): self;

    /** Indicates that variable is initially an array of items. */
    public function array(bool $array = true): self;

    /**
     * Set variable category. Variables are grouped by category.
     *
     * @param string|VariableCategory $value Localized category name or internal category
     */
    public function category(string|VariableCategory $value): self;

    /**
     * Specify one or more flags that must be set in order for this variable to be available.
     * Check documentation for available flags.
     */
    public function contextFlags(array|string $value): self;

    /** Changes availability for conditions editor in apps interface. */
    public function hideFromConditions(bool $value = true): self;
}