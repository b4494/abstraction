<?php

namespace BmPlatform\Abstraction\Interfaces;

use JetBrains\PhpStorm\ArrayShape;

interface VariableRegistrar
{
    /** Start a group of variables. Each option will prefix corresponding option for variable. */
    public function group(
        #[ArrayShape([
            'key' => 'string',
            'checkContext' => 'array',
            'name' => 'string',
            'category' => 'string',
        ])]
        array $group,
        callable $callback
    ): void;

    /** Register aliases for variables */
    public function alias(string|array $abstract, ?string $concrete = null);

    public function text(string $key, string|callable|null $resolver = null): Variable;

    public function mixed(string $key, string|callable|null $resolver = null): Variable;

    public function number(string $key, string|callable|null $resolver = null): Variable;

    public function int(string $key, string|callable|null $resolver = null): Variable;

    public function float(string $key, string|callable|null $resolver = null): Variable;

    public function bool(string $key, string|callable|null $resolver = null): Variable;

    public function datetime(string $key, string|callable|null $resolver = null): Variable;

    public function date(string $key, string|callable|null $resolver = null): Variable;

    public function time(string $key, string|callable|null $resolver = null): Variable;

    public function money(string $key, string|callable|null $resolver = null): Variable;

    public function phone(string $key, string|callable|null $resolver = null): Variable;

    public function pk(string $key, string|callable|null $resolver = null): Variable;

    /**
     * @param string $key
     * @param array<int, array{value: string, label: string}>|callable $options
     * @param string|callable|null $resolver
     *
     * @return \BmPlatform\Abstraction\Interfaces\Variable
     */
    public function enum(string $key, array|callable $options, string|callable|null $resolver = null): Variable;

    /**
     * Register a complex type that has its properties. Inner variables are resolved relative to the parent variable.
     * I.e. to resolve `foo.bar`, `foo` is resolved first and then passed to resolver of `bar`.
     *
     * @param string $key
     * @param callable $callback Accepts variables registrar
     * @param string|callable|null $resolver
     *
     * @return \BmPlatform\Abstraction\Interfaces\Variable
     */
    public function complex(string $key, callable $callback, string|callable|null $resolver = null): Variable;

    /**
     * @param string $key
     * @param string|callable|null $resolver Resolver must return string|BmPlatform\Abstraction\DataTypes\Tag or array of those
     *
     * @return \BmPlatform\Abstraction\Interfaces\Variable
     */
    public function tag(string $key, string|callable|null $resolver = null): Variable;
}