<?php

namespace BmPlatform\Abstraction\Requests;

use BmPlatform\Abstraction\Interfaces\Chat;

class BaseMessageRequest
{
    public function __construct(
        public readonly Chat $chat,

        /** @var \BmPlatform\Abstraction\DataTypes\QuickReply[][] */
        public readonly array $quickReplies = [],

        /** @var \BmPlatform\Abstraction\DataTypes\InlineButton[][] */
        public readonly array $inlineButtons = [],
    ) {
        //
    }
}