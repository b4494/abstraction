<?php

namespace BmPlatform\Abstraction\Requests;

use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Interfaces\Tag;

class ChatTagRequest
{
    public function __construct(
        public readonly Chat       $chat,
        public readonly Tag|string $tag,
        public readonly bool       $forTicket = false,
    ) {
        //
    }
}