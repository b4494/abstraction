<?php

namespace BmPlatform\Abstraction\Requests;

use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\Interfaces\Chat;

class SendMediaRequest extends BaseMessageRequest
{
    public function __construct(
                        Chat      $chat,
        public readonly MediaFile $file,
        public readonly ?string   $caption = null,
                        array     $quickReplies = [],
                        array     $inlineButtons = [],
    ) {
        parent::__construct($chat, $quickReplies, $inlineButtons);
    }
}