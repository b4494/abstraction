<?php

namespace BmPlatform\Abstraction\Requests;

use BmPlatform\Abstraction\Interfaces\Chat;

class SendSystemMessageRequest
{
    public function __construct(public readonly Chat $chat, public readonly string $text)
    {
        //
    }
}