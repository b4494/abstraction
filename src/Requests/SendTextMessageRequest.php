<?php

namespace BmPlatform\Abstraction\Requests;

use BmPlatform\Abstraction\Interfaces\Chat;

class SendTextMessageRequest extends BaseMessageRequest
{
    public function __construct(
                        Chat   $chat,
        public readonly string $text,
                        array  $quickReplies = [],
                        array  $inlineButtons = [],
    ) {
        parent::__construct($chat, $quickReplies, $inlineButtons);
    }
}