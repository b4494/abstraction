<?php

namespace BmPlatform\Abstraction\Requests;

use BmPlatform\Abstraction\Interfaces\OperatorGroup;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Interfaces\Operator;

class TransferChatToOperatorRequest
{
    public function __construct(public readonly Chat $chat, public readonly Operator|OperatorGroup $target)
    {
        //
    }
}