<?php

namespace BmPlatform\Abstraction\Requests;

use BmPlatform\Abstraction\Interfaces\Contact;

readonly class UpdateContactDataRequest
{
    public function __construct(
        public Contact $contact,

        /** Data array is made of properties defined in `actions.update_contact_data.editableFields` schema property. */
        public array   $data,

        /** @depreacted  */
        public ?string $name = null,

        /** @depreacted  */
        public ?string $comment = null,

        /** @depreacted  */
        public ?string $phone = null,

        /** @depreacted  */
        public ?string $email = null,

        /** @depreacted  */
        public ?array  $fields = null,
    ) {
        //
    }
}