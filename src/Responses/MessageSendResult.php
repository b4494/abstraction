<?php

namespace BmPlatform\Abstraction\Responses;

class MessageSendResult
{
    public function __construct(
        public readonly string  $externalId,
        public readonly ?string $messengerId = null,
    ) {
        //
    }
}