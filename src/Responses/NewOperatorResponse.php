<?php

namespace BmPlatform\Abstraction\Responses;

use BmPlatform\Abstraction\DataTypes\Operator;

class NewOperatorResponse
{
    public function __construct(public readonly Operator|string $operator)
    {
        //
    }
}